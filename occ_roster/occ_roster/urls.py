from django.contrib import admin
from django.urls import path

from roster_app import views as roster_app_views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("shifts", roster_app_views.shift_list),
]
