from django.db import models


class Shift(models.Model):
    name = models.CharField(max_length=200)
    start = models.DateTimeField("start time", t)
    end = models.DateTimeField("end time")

    def __str__(self):
        return f"{self.start:%d/%m/%y %H:%M}-{self.end:%H:%M} - {self.name}"
