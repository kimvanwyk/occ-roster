from django.shortcuts import render
from rich import print

from . import utils
from . import models


def shift_list(request):
    starts = utils.get_next_week_shift_times()
    shifts = []
    for start in starts:
        shifts.extend(models.Shift.objects.filter(start=start))
    print(shifts)
    return render(request, "shifts.html")
