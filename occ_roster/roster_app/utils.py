from datetime import datetime, timedelta

from rich import print

SHIFT_LENGTH = 2


def get_next_week_shift_times():
    """Return start datetimes for the next week of shifts, to midnight of 6 days from now"""
    now = datetime.now() + timedelta(hours=2)
    # Won't work for odd start hours or most divisors
    h = now.hour + (now.hour % SHIFT_LENGTH)
    if h >= 24:
        h = 0
    start = now.replace(hour=h, minute=0, second=0, microsecond=0)
    end = (start + timedelta(days=7)).replace(hour=0, minute=0, second=0, microsecond=0)

    shifts = []
    s = start
    while s < end:
        shifts.append(s)
        s += timedelta(hours=SHIFT_LENGTH)
    return shifts


if __name__ == "__main__":
    print(get_next_week_shift_times())
