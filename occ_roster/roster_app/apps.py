from django.apps import AppConfig


class RosterAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "roster_app"
